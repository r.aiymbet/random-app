//
//  ContentView.swift
//  SoundLib
//
//  Created by Raiymbek Aiymbet on 2/21/21.
//

import SwiftUI
import Firebase

struct Collection: Hashable {
    var title: String
    var image: String
    var audioBooks: [AudioBook]
}

struct AudioBook: Hashable {
    var title: String
    var time: String
}

struct ContentView: View {
    var collections = [Collection(title: "Romance", image: "1", audioBooks: [
                    AudioBook(title: "Pride and Prejudice", time: "1:20:30"),
                    AudioBook(title: "Jane Eyre", time: "7:20:30"),
                    AudioBook(title: "Outlander", time: "4:20:30"),
                    AudioBook(title: "The Duke and I", time: "1:50:30")]),
                Collection(title: "Fantasy", image: "2", audioBooks: [
                    AudioBook(title: "The Hobbit", time: "4:20:30"),
                    AudioBook(title: "A Storm of Swords", time: "1:50:30"),
                    AudioBook(title: "The Last Unicorn", time: "1:20:30"),
                    AudioBook(title: "The Way of Kings", time: "4:20:30")]),
                Collection(title: "Historical", image: "3", audioBooks: [
                    AudioBook(title: "War and Peace", time: "1:20:30"),
                    AudioBook(title: "The Book Thief", time: "1:20:30"),
                    AudioBook(title: "Wolf Hall", time: "1:50:30"),
                    AudioBook(title: "I, Claudius", time: "7:20:30")]),
                Collection(title: "HP Series", image: "4", audioBooks: [
                    AudioBook(title: "Harry Potter and the Sorcerer’s Stone", time: "15:20:30"),
                    AudioBook(title: "Harry Potter and the Chamber of Secrets", time: "1:50:30"),
                    AudioBook(title: "Harry Potter and the Prisoner of Azkaban", time: "1:20:30"),
                    AudioBook(title: "Harry Potter and the Goblet of Fire", time: "7:20:30"),
                    AudioBook(title: "Harry Potter and the Order of the Phoenix", time: "1:20:30"),
                    AudioBook(title: "Harry Potter and the Half-Blood Prince", time: "1:20:30"),
                    AudioBook(title: "Harry Potter and the Deathly Hallows", time: "1:20:30"),
                    AudioBook(title: "Harry Potter and the Cursed Child", time: "1:20:30")
                ]),
                Collection(title: "Sci-Fi", image: "5", audioBooks: [
                    AudioBook(title: "Dune", time: "7:20:30"),
                    AudioBook(title: "Neuromancer", time: "1:50:30"),
                    AudioBook(title: "Snow Crash", time: "1:20:30"),
                    AudioBook(title: "Fahrenheit 451", time: "7:20:30")])]
    
    @State private var currentCollection: Collection?
        
    var body: some View {
        NavigationView {
            ScrollView {
                ScrollView(.horizontal, content: {
                    LazyHStack {
                        ForEach(self.collections, id: \.self, content: {
                            collection in
                            CollectionPoster(collection: collection, alongWithText: true).onTapGesture {
                                self.currentCollection = collection
                            }
                        })
                    }
                })
                LazyVStack {
                    ForEach((self.currentCollection?.audioBooks ?? self.collections.first?.audioBooks) ?? [
                                AudioBook(title: "Book 1", time: "12:25:25"),
                                AudioBook(title: "Book 2", time: "2:50:00"),
                                AudioBook(title: "Book 3", time: "23:27:40"),
                                AudioBook(title: "Book 4", time: "10:15:30")],
                            id: \.self,
                            content: {
                                audioBook in audioBookCell(collection: currentCollection ?? collections.first!, audioBook: audioBook)
                            })
                }
            }.navigationTitle("Audio Collections")
        }
    }
}

struct CollectionPoster: View {
    var collection: Collection
    var alongWithText: Bool
    var body: some View {
        ZStack(alignment: .bottom, content: {
            Image(collection.image).resizable().aspectRatio(contentMode: .fill).frame(width: 170, height: 200, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            
            if alongWithText == true {
            ZStack {
                Blur(style: .dark)
                Text(collection.title).foregroundColor(.white)
            }.frame(height: 60, alignment: .center)
            }
            
        }).frame(width: 170, height: 200, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).clipped().cornerRadius(20).padding(20)
    }
}

struct audioBookCell: View {
    var collection: Collection
    var audioBook: AudioBook
    var body: some View {
        NavigationLink(
            destination: PlayerView(collection: collection, audioBook: audioBook),
            label: {
                HStack {
                    ZStack {
                        //Circle().frame(width: 40, height: 40, alignment: .center).foregroundColor(.red)
                        //Circle().frame(width: 20, height: 20, alignment: .center).foregroundColor(.white)
                        //Image(systemName: "book.closed").font(.system(size: 30)).foregroundColor(Color(UIColor.brown))
                    }
                    Image(systemName: "book.closed").font(.system(size: 30)).foregroundColor(Color(UIColor.brown))
                    Text(audioBook.title).bold()
                    Spacer()
                    Text(audioBook.time)
                }.padding(20)
            }
        ).buttonStyle(PlainButtonStyle())
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
