//
//  SoundLibApp.swift
//  SoundLib
//
//  Created by Raiymbek Aiymbet on 2/24/21.
//

import SwiftUI
import Firebase

@main
struct SoundLibApp: App {
    
    init() {
        FirebaseApp.configure()
    }
    
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
