//
//  PlayerView.swift
//  SoundLib
//
//  Created by Raiymbek Aiymbet on 2/23/21.
//

import Foundation
import SwiftUI

struct PlayerView: View {
    
    let color = UIColor(red: 219, green: 186, blue: 125, alpha: 1)
    var collection: Collection
    var audioBook: AudioBook
    
    @State var isPlaying: Bool = false
    
    var body: some View {
        ZStack {
            Image(collection.image).resizable().edgesIgnoringSafeArea(.all)
            //Blur(style: .dark).edgesIgnoringSafeArea(.all)
            VStack {
                Spacer()
                //CollectionPoster(collection: collection, alongWithText: false)
                Text(audioBook.title).font(.title).fontWeight(.heavy).foregroundColor(.white)
                //Text(song.name)
                Spacer()
                ZStack{
                    Color.yellow.cornerRadius(20).shadow(radius: 10)
                    //Color(red: 255, green: 222, blue: 166).cornerRadius(20).shadow(radius: 10)
                    
                    HStack {
                        Button(action: self.previous, label: {
                            Image(systemName: "backward.frame").resizable()
                        }).frame(width: 40, height: 40, alignment: .center)
                        //Spacer()
                        Button(action: self.playPause, label: {
                            Image(systemName: isPlaying ? "pause.circle.fill" : "play.circle.fill").resizable()
                        }).frame(width: 70, height: 70, alignment: .center).padding()
                        //Spacer()
                        Button(action: self.next, label: {
                            Image(systemName: "forward.frame").resizable()
                        }).frame(width: 40, height: 40, alignment: .center)
                    }
                }.edgesIgnoringSafeArea(.bottom).frame(height: 100, alignment: .center)
            }
        }
    }
    
    func playPause() {
        self.isPlaying.toggle()
    }
    
    func next() {
        
    }
    
    func previous() {
        
    }
}
